#!/bin/bash

# Global variables
AV_SCAN_FILE="av-scan-file.txt"
/bin/echo "===================" > /tmp/$AV_SCAN_FILE

return_code=0

# Check for root privileges
if [[ $EUID -ne 0 ]]; then
    /bin/echo "Error! virus scan: only root can do that" >> /tmp/$AV_SCAN_FILE
    /bin/mail -s "[clamav] Virus Scan Errror  @ EARTH.SOFIBOX.COM" webmaster@sofibox.com < /tmp/$AV_SCAN_FILE
    exit 1
fi

# Display initializing message
/bin/echo "" >> /tmp/$AV_SCAN_FILE
/bin/echo "Initializing virus scan..." >> /tmp/$AV_SCAN_FILE
/bin/echo "" >> /tmp/$AV_SCAN_FILE

# Prepare virus scan log
/bin/sudo rm -f /var/log/virus-scan.log
/bin/sudo touch /var/log/virus-scan.log
/bin/sudo chown root:adm /var/log/virus-scan.log
/bin/sudo chmod 640 /var/log/virus-scan.log

# Display updating message
/bin/echo "Updating virus definitions..." >> /tmp/$AV_SCAN_FILE
/bin/echo ""  >> /tmp/$AV_SCAN_FILE
/bin/sudo /usr/local/bin/freshclam >> /tmp/$AV_SCAN_FILE
return_code=$?

# Display updating failed status message and exit, if update failed
if [ $return_code -ne 0 ]; then
    /bin/echo "" >> /tmp/$AV_SCAN_FILE
    /bin/echo "Failed to updating virus definitions... Virus scan aborted" >> /tmp/$AV_SCAN_FILE
    /bin/echo "" >> /tmp/$AV_SCAN_FILE
    /bin/mail -s "[clamav] Failed to update virus definition  @ EARTH.SOFIBOX.COM" webmaster@sofibox.com < /tmp/$AV_SCAN_FILE
    exit $return_code
fi

# Commence virus scan
/bin/echo "Update completed" >> /tmp/$AV_SCAN_FILE
/bin/echo "" >> /tmp/$AV_SCAN_FILE
/bin/echo "Commencing virus scan... (this may take some time)" >> /tmp/$AV_SCAN_FILE
/bin/echo "" >> /tmp/$AV_SCAN_FILE
/bin/sudo /usr/local/bin/clamscan -r / --log='/var/log/virus-scan.log' --exclude='^/var/log/virus-scan\.log$' --exclude-dir='^/sys'
return_code=$?

# Display virus scan status message
if [ $return_code -ne 0 ] && [ $return_code -ne 1 ]; then
    /bin/echo "" >> /tmp/$AV_SCAN_FILE
   /bin/echo "Failed to complete virus scan" >> /tmp/$AV_SCAN_FILE
   /bin/echo "" >> /tmp/$AV_SCAN_FILE
   /bin/mail -s "[clamav] Failed to complete virus scan  @ EARTH.SOFIBOX.COM" webmaster@sofibox.com < /tmp/$AV_SCAN_FILE
else
    /bin/echo "" >> /tmp/$AV_SCAN_FILE
    /bin/echo -n "Virus scan completed successfully, " >> /tmp/$AV_SCAN_FILE
    if sudo grep -rl 'Infected files: 0' /var/log/virus-scan.log >> /tmp/$AV_SCAN_FILE; then
        /bin/echo "NO INFECTIONS FOUND" >> /tmp/$AV_SCAN_FILE
        /bin/echo "" >> /tmp/$AV_SCAN_FILE
         /bin/mail -s "[clamav] NO INFECTIONS FOUND @ EARTH.SOFIBOX.COM" webmaster@sofibox.com < /tmp/$AV_SCAN_FILE
 else
        sudo grep -rl 'Infected files' /var/log/virus-scan.log >> /tmp/$AV_SCAN_FILE
        /bin/echo "INFECTIONS FOUND (please review the '/var/log/virus-scan.log' file)" >> /tmp/$AV_SCAN_FILE
       /bin/echo "" >> /tmp/$AV_SCAN_FILE
     /bin/mail -s "[clamav] ONE OR MORE INFECTIONS FOUND @ EARTH.SOFIBOX.COM" webmaster@sofibox.com < /tmp/$AV_SCAN_FILE
    fi
fi

/bin/echo "[clamav] Done checking virus .. email is set to be sent to webmaster@sofibox.com"
/bin/echco "========================="

exit $return_code
